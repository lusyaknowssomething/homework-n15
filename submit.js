const form = document.querySelector("#myForm");
const hiddenInput = form.querySelector(".hidden-input");
const submitMessage = form.querySelector(".subit-message");

async function sendData(data) {
  return await fetch("/posturl/", {
    method: "POST",
    headers: { "Content-Type": "multipart/form-data" },
    body: data,
  });
}

function getFormData(data) {
  return new FormData(data);
}

async function handleSubmit(e) {
  e.preventDefault();

  if (form.querySelector(".new-input")) {
    console.log('поле уже добавлено');
  } else {
    newInput = document.createElement("input");
    newInput.classList.add("new-input");
    newInput.type = "hidden";
    newInput.name = "newField";
    newInput.value = "customValue";
    form.querySelector(".button").before(newInput);
  }

  if (hiddenInput) {
    hiddenInput.remove();
  }

  const data = getFormData(form);
  const { status } = await sendData(data);
  if (status === 200) {
    submitMessage.classList.add("subit-message_type_success");
    submitMessage.textContent = "Форма отправлена!";
  } else {
    submitMessage.classList.add("subit-message_type_fail");
    submitMessage.textContent = "Что-то пошло не так :(";
  }
}

form.addEventListener("submit", handleSubmit);
